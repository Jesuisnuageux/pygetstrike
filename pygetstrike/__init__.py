#!/usr/bin/python

"""
	Author : jesuisnuageux
	Date : 20/03/2015
	Function : Simple wrapper for getstrike.net's API
"""

from platform import python_version
from sys import exit

if python_version().split('.') > ['3', '0', '0']:
    from pygetstrike.api_v1 import Api as ApiV1
    from pygetstrike.api_v2 import Api as ApiV2
else:
    print("Sorry, Python3 is needed for this")
    exit()
