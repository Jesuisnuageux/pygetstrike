#!/usr/bin/python
"""
	Author : jesuisnuageux
	Date : 20/03/2015
	Function : Simple object to store torrent's data
"""


class Torrent:
    def __init__(self, data):
        self.hash = data['torrent_hash']
        self.title = data['torrent_title']
        self.category = data['torrent_category']
        self.subcategory = data['sub_category']
        self.seeds = data['seeds']
        self.leeches = data['leeches']
        self.file_count = data['file_count']
        self.size = data['size']
        self.upload_date = data['upload_date']
        self.upload_username = data['uploader_username']
        self.magnet_uri = ""
        self.page = ""
        self.rss_feed = ""
        # in case of API v2
        if 'magnet_uri' in data:
            self.magnet_uri = data['magnet_uri']
        if 'page' in data:
            self.page = data['page']
        if 'rss_feed' in data:
            self.rss_feed = data['rss_feed']
        return

    def __str__(self):
        return "{}".format(self.title)

    def __unicode__(self):
        return u"{}".format(self.title)

    def info(self):
        return """
		title : {title}
		hash : {hash}
		category : {cat}
		subcategory : {sbcat}
		seeds : {seeds}
		leeches : {leeches}
		file_count : {fcount}
		size : {size}
		upload date : {udate}
		uploader username : {uname}
		magnet uri : {magnet}
		page : {page}
		rss feed : {rss}
		""".format(title=self.title, hash=self.hash, cat=self.category, sbcat=self.subcategory, seeds=self.seeds,
                   leeches=self.leeches, fcount=self.file_count, size=self.size, udate=self.upload_date,
                   uname=self.upload_username, magnet=self.magnet_uri, page=self.page, rss=self.rss_feed)
