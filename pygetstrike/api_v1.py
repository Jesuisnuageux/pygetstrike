#!/usr/bin/python


from urllib import parse, request, error
import json
import os

from pygetstrike.torrent import Torrent


"""
	Author : jesuisnuageux
	Date : 20/03/2015
	Function : Api object for python3, handling basic API function : search + download torrent file
"""


class Api:
    def __init__(self, verbose=False):
        self.download_url = "http://getstrike.net/api/torrents/downloads/?"
        self.search_url = "https://getstrike.net/api/torrents/search/?"
        self.info_url = "http://getstrike.net/api/torrents/info/?"
        # self.results handles torrent objects to be able to print and download them later
        self.results = []
        self.verbose = verbose
        return

    def _print(self, txt):
        if self.verbose:
            print("[DEBUG] "+txt)
        return

    def search(self, query):
        """
			param : keyword you looking for
			return : response status code
		"""
        self.results = []
        self._print("url = "+self.search_url + parse.urlencode({"q": query}))
        try:
            data = json.loads(
                request.urlopen(self.search_url + parse.urlencode({"q": query})).readall().decode('utf-8'))
        except error.HTTPError as e:
            print(">> {} <<".format(e.__str__()))
            return
        if data[0]['statuscode'] != 200:
            self._print("> Status code returned is {}".format(data[0]['statuscode']))
        else:
            self.results = []
            for i in range(data[0]['results']):
                self.results.append(Torrent(data[1][i]))
        return data[0]['statuscode']

    def get_info(self,hashes):
        self._print("url = "+self.info_url + parse.urlencode({"hashes":hashes}))
        try:
            data = json.loads( request.urlopen(self.info_url + parse.urlencode({"hashes":hashes})).readall().decode('utf-8'))
        except error.HTTPError as e:
            print(">> {} <<".format(e.__str__()))
            return
        if data[0]['statuscode'] == 200:
            self.results=[]
        return Torrent(data[1][0])


    def print_results(self,limit=100):
        print(">> Resultats <<")
        for i in range(min(limit,len(self.results))):
            print("{} : {:50} (seed={:>6}/leech={:>6})".format(i, self.results[i].title, self.results[i].seeds, self.results[i].leeches))
        return 1

    def __str__(self):
        return "{}".format(self.print_results())

    def __unicode__(self):
        return u"{}".format(self.print_results())

    def get_result(self,index):
        if index>=0 and index<len(self.results):
            return self.results[index]
        else:
            return None

    def get_url(self, hash):
        """
			param : index of torrent you want to dl in self.results list
			return : download link if successful, None if something wrong happened
		"""
        self._print("url = "+self.download_url + parse.urlencode({"hash":hash}))
        data = json.loads(
            request.urlopen(self.download_url + parse.urlencode({"hash":hash})).readall().decode(
                'utf-8'))
        if data['statuscode'] == 200:
            return data['message']
        else:
            return None

    def download_torrent_file(self, index):
        """
			param : index of torrent you want to dl in self.results list
			return : name of the torrent file you just downloaded
		"""
        with open(self.results[index].hash + ".torrent", "b+w") as torrent_file:
            torrent_file.write(request.urlopen(self.get_url(index)).read())
        return "{}.torrent".format(self.results[index].hash)
