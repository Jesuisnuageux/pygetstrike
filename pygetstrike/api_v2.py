#!/usr/bin/python
import base64

from urllib import parse, request, error
import json
import os

from pygetstrike.torrent import Torrent


"""
	Author : jesuisnuageux
	Date : 26/03/2015
	Function : Api object for python3, adding support API v2
"""


class Api:
    def __init__(self, verbose=False):
        self.download_url = "https://getstrike.net/api/v2/torrents/download/?"
        self.description_url = "https://getstrike.net/api/v2/torrents/descriptions/?"
        self.search_url = "https://getstrike.net/api/v2/torrents/search/?"
        self.info_url = "https://getstrike.net/api/v2/torrents/info/?"
        self.count_url = "https://getstrike.net/api/v2/torrents/count/"
        self.category = ["Anime","Applications","Books","Games","Movies","Music","Other","TV","XXX"]
        self.subcategory = ["Highres Movies","Hentai","HD Video","Handheld","Games","Fiction","English-translated","Ebooks","Dubbed Movies","Documentary","Concerts","Comics","Books","Bollywood","Audio books","Asian","Anime Music Video","Animation","Android","Academic","AAC","3D Movies","XBOX360","Windows","Wii","Wallpapers","Video","Unsorted","UNIX","UltraHD","Tutorials","Transcode","Trailer","Textbooks","Subtitles","Soundtrack","Sound clips","Radio Shows","PSP","PS3","PS2","Poetry","Pictures","PC","Other XXX","Other TV","Other Music","Other Movies","Other Games","Other Books","Other Applications","Other Anime","Non-fiction","Newspapers","Music videos","Mp3","Movie clips","Magazines","Mac","Lossless","Linux","Karaoke","iOS"]
        # self.results handles torrent objects from latest search
        self.results = []
        self.verbose = verbose
        return

    def _print(self, txt):
        if self.verbose:
            print("[DEBUG] "+txt)
        return

    def search(self, query):
        """
			param : keyword you looking for
			return : response status code
		"""
        self.results = []
        self._print(self.search_url + parse.urlencode({"phrase": query}))
        try:
            data = json.loads(
                request.urlopen(self.search_url + parse.urlencode({"phrase": query})).readall().decode('utf-8'))
        except error.HTTPError as e:
            print(">> {} <<".format(e.__str__()))
            return
        if data['statuscode'] != 200:
            self._print("> Status code returned is {}".format(data['statuscode']))
        else:
            self.results = []
            for i in range(data['results']):
                self.results.append(Torrent(data['torrents'][i]))
        return data['statuscode']

    def get_info(self,hash):
        self._print(self.info_url + parse.urlencode({"hashes":hash}))
        try:
            data = json.loads( request.urlopen(self.info_url + parse.urlencode({"hashes":hash})).readall().decode('utf-8'))
        except error.HTTPError as e:
            print(">> {} <<".format(e.__str__()))
            return
        if data['statuscode'] == 200:
            self.results=[]
        return Torrent(data['torrents'][0])

    def get_desc(self,hash):
        self._print(self.description_url + parse.urlencode({"hash":hash}))
        try:
            data = json.loads( request.urlopen(self.description_url + parse.urlencode({"hash":hash})).readall().decode('utf-8'))
        except error.HTTPError as e:
            print(">> {} <<".format(e.__str__()))
            return
        if data['statuscode'] == 200:
            return str(base64.standard_b64decode(data['message']))[2:-1]
        else:
            return None


    def get_result(self,index):
        if index>=0 and index<len(self.results):
            return self.results[index]
        else:
            return None

    def print_results(self, limit=-1):
        print(">> Resultats <<")
        for i in range(min(limit,len(self.results))):
            print("{} : {:50} (seed={:>6}/leech={:>6})".format(i, self.results[i].title, self.results[i].seeds, self.results[i].leeches))
        return 1

    def __str__(self):
        return "{}".format(self.print_results())

    def __unicode__(self):
        return u"{}".format(self.print_results())

    def get_url(self, hashes):
        """
			param : index of torrent you want to dl in self.results list
			return : download link if successful, None if something wrong happened
		"""
        self._print(self.download_url + parse.urlencode({"hash":hashes}))
        data = json.loads(
            request.urlopen(self.download_url + parse.urlencode({"hash":hashes})).readall().decode(
                'utf-8'))
        if data['statuscode'] == 200:
            return data['message']
        else:
            return None

    def download_torrent_file(self, index):
        """
			param : index of torrent you want to dl in self.results list
			return : name of the torrent file you just downloaded
		"""
        with open(self.results[index].hash + ".torrent", "b+w") as torrent_file:
            torrent_file.write(request.urlopen(self.get_url(index)).read())
        return "{}.torrent".format(self.results[index].hash)
