### How to use this thing : ###

```
#!python


# importing lib
import pygetstrike as gsk
# instanciation of an Api object
api = gsk.Api()

# Once all this is done, you can do whatever you want :

# search for something, print the results and download the .torrent you want ( it'll be the 5th one for me )
api.search("citizenfour")
api.print_results()
api.download(4)

# but you can also get all information about a torrent from its hash
api.getInfo("B0062EA9C009017FFEA453F683508EEBB4DC0BD8").info()

# Or get those informations from any of your search
api.results[4].info()
```