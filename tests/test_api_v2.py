from pygetstrike.api_v2 import Api as ApiV2

"""
    Author : jesuisnuageux
    Date : 26/03/2015
    Function : test api v2 and show how to use api v2
"""

if __name__=="__main__":
    api = ApiV2(True)

    print("%% Searching for 'manjaro'")
    api.search("manjaro")
    print("%% Get {} results :".format(len(api.results)))
    print("%% Printing results :")
    api.print_results(10)
    print("%% First torrent's info :")
    torrent = api.get_result(0)
    if not torrent is None:
        print(api.get_info(torrent.hash).info())
        print("%% First torrent's description :")
        print(api.get_desc(torrent.hash))
        print("%% First torrent's magnet link :")
        print(torrent.magnet_uri)
        print("%% First torrent's download link :")
        print(api.get_url(torrent.hash))


