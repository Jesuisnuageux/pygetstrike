from pygetstrike.api_v1 import Api as ApiV1

"""
    Author : jesuisnuageux
    Date : 26/03/2015
    Function : test api v1 and show how to use api v1
"""

if __name__=="__main__":
    api = ApiV1(True)

    print("%% Searching for 'manjaro'")
    api.search("manjaro")
    print("%% Get {} results :".format(len(api.results)))
    print("%% Printing results :")
    api.print_results(10)
    torrent = api.get_result(0)
    if not torrent is None:
        print("%% First torrent's info :")
        print(api.get_info(torrent.hash).info())
        print("%% First torrent's download link :")
        print(api.get_url(torrent.hash))


